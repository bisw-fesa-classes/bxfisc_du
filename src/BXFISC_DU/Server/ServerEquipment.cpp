#include <BXFISC_DU/Server/ServerEquipment.h>
#include <BXFISC/GeneratedCode/ServiceLocator.h>
namespace BXFISC_DU
{

ServerEquipment::ServerEquipment (const std::map<std::string, fesa::AbstractServiceLocator*>& serviceLocatorCol) : ServerEquipmentGen(serviceLocatorCol)
{
}

ServerEquipment::~ServerEquipment ()
{
}

void ServerEquipment:: specificInit () 
{
}

}
