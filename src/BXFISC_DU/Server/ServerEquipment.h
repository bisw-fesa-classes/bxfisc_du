#ifndef BXFISC_DU_SERVER_EQUIPMENT_H_
#define BXFISC_DU_SERVER_EQUIPMENT_H_

#include <fesa-core/Server/AbstractServerEquipment.h>

#include <fesa-core/Exception/FesaException.h>

#include <BXFISC_DU/GeneratedCode/ServerEquipmentGen.h>

namespace BXFISC_DU
{

class ServerEquipment : public ServerEquipmentGen
{
	public:

		ServerEquipment (const std::map<std::string, fesa::AbstractServiceLocator*>& serviceLocatorCol);

		virtual  ~ServerEquipment();

		void specificInit();

};
}
#endif
